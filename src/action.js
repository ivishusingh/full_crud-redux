

export const AddData = (name, number) => {
    return {
        type: 'ADD_DATA',
        payload: {
            Name: name,
            Number: number
        }
    };
};

export const EditData = (i) => {
    return {
        type: 'EDIT_DATA',
        payload: i
    }
}

export const DeleteData = (i) => {
    return{
        type: 'DELETE_DATA',
        payload: i
    }
}