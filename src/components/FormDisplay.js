import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import {  EditData, DeleteData } from '../action';


class FormDisplay extends React.Component{

    render(){
        return (
            <div>
              <table className="table">
                <thead className="thead-dark">
                  <tr>
                    <th scope="col">NAME</th>
                    <th scope="col">NUMBER</th>
                  </tr>
                </thead>
                <tbody>
                  {this.props.Name.map((item, i) => (
                    <tr key={i}>
                      <td>{item.Name}</td>
                      <td>{item.Number}</td>
                      <td>
                        <button onClick={() => this.props.EditData(i)} className="btn btn-success">Edit</button>
                        <button onClick={() => this.props.DeleteData(i)} className="btn btn-danger" >Delete</button>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
        
              <button>
                <Link to="/" className="item">
                  AddData
                </Link>
              </button>
            </div>
          );
    }
}


const mapStateToProps = state => {
    console.log(state.data)
  return {
    Name: state.data
  };
};

export default connect(mapStateToProps,{EditData,DeleteData})(FormDisplay);
