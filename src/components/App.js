import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import FormAdd from './FormAdd';
import FormDisplay from './FormDisplay';
import Header from './Header';

const App = () => {
    return (
        <div className="ui container">
            <BrowserRouter>
                <div>
                < Header />
                    <Route path="/" exact component={FormAdd} />
                    <Route path="/Display" exact component={FormDisplay} />

                </div>
            </BrowserRouter>
        </div>
    );
};

export default App;

