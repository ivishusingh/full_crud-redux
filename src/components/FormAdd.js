import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { AddData } from '../action';

class FormAdd extends React.Component {
    state = { Name: "", Number: "" }



    render() {
        return (
            <div>
                <form className="form-group">
                    <h3>Enter Data Here</h3><br />

                    <label>Enter Name</label>
                    <input type="text" className="form-control" onChange={(e) => this.setState({ Name: e.target.value })} />
                    <br /><br />

                    <label>Enter Number</label>
                    <input type="number" className="form-control" onChange={(e) => this.setState({ Number: e.target.value })} />
                    <br /><br />


                    <button onClick={() => this.props.AddData(this.state.Name, this.state.Number)} >
                        <Link to="/Display" className="item">
                            Submit Data
                     </Link>
                    </button>
                </form>
            </div>
        );
    }

}


const mapStateToProps = (state) => {
    return { Data: state.Data }
}

export default connect(mapStateToProps, { AddData })(FormAdd);
