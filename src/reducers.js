let initialState = {
  data: [],
  products: []
};

const Data = (state = initialState, action) => {
  switch (action.type) {
    case "ADD_DATA":
      return {
        ...state,
        data: state.data.concat(action.payload)
      };

    case "DELETE_DATA":
      let productscopy = [...state.data];
      let updatedArray = productscopy.filter((item, i) => action.payload !== i);
      return {
        ...state,
        data: updatedArray
      };

    default:
      return state;
  }
};

export default Data;
